\name{NEWS}
\title{NEWS}

\section{Changes in version 1.5.0}{
\itemize{
\item Added \code{clean_weham()} to remove output and working files.
\item We now import \code{fritools::is_windows()}.
\item \code{read_access_table()} will now import the whole database into a named list if
you pass \code{table_name = NULL}.
\item Removed gitlab-ci, as gitlab will not run WEHAM, so the continuous integration
will always fail.
\item Now function \code{bulk_export_access_tables()} throws an error if you changed more
than one table \emph{and} pass a primary key.
\item Fixed link in documentation for \code{weham()}.
}
}

\section{Changes in version 1.4.0}{
\itemize{
\item Now using weham3.1.2!
WEHAM now reads full file paths from it's ini file, allowing for different
names for input, control and model like
_WEHAM\\emph{Steuer=c:/tmp/foo.mdb}, as long as the end in \emph{.mdb}.
The ini file itself must no longer be  \emph{./weham.ini} but may be
\emph{/path/to/some/file}.
See  \url{https://git-dmz.thuenen.de/cullmann/weham//-/blob/master/NEWS.md}
for details on weham3.1.2
}
}

\section{Changes in version 1.3.0}{
\emph{The backward compatiblity for functions \code{bulk_import_access_tables()} and
\code{bulk_import_access_tables()} introduced in 1.2.0 is broken!}
\subsection{Changed Interfaces}{
\itemize{
\item \code{bulk_import_access_tables()} does not write to \code{.GlobalEnv} anymore, thus returning a list of data and meta data.
output of \code{bulk_import_access_tables()}.
\item \code{bulk_export_access_tables()} now reads the changed return value from \code{bulk_import_access_tables()} \code{bulk_import_access_tables()}.
}
}

\subsection{The Name of the Control}{

The (base)name of the control database ("weham\_steuerXX.mdb") needs to be passed to
WEHAM, it cannot be specified in file "weham.ini" and may vary in "XX", in
contrary to the input database and the model database which invariably need to
be "weham\_input.mdb" and "weham\_modell.mdb".
Now \code{guess_control()} (called by \code{weham()} by default) tries to read an option
"weham_control_name" by default. This option is set by
\code{set_ini_attribute()} (called by \code{create_weham_ini()})
if the attribute set is "WEHAM_Steuer".
To specify the name of the control database you can
\itemize{
\item pass it directly to \code{weham()} via the argument \code{control_name},
\item set the option "weham_control_name" and stick with the default argument
\code{control_name} in the call to \code{weham()}.
\item set the attribute "WEHAM_Steuer" via \code{set_ini_attribute()} (possibly via
\code{create_weham_ini()} and sticking with the default argument
\code{control_name} in the call to \code{weham()}.
}
}

\subsection{Minor Changes}{
\itemize{
\item New convenience functions \code{get_data()} and \code{get_metadata()} to digest the
\item Removed argument \code{verbose} from \code{bulk_import_access_tables()}
\item Now \code{bulk_import_access_tables()} by default labels weham databases:
\itemize{
\item weham\_steuerXX is labeled "control"
\item weham\_input is labeled "input"
\item weham\_modell is labeled "model"
The labels are use as names in the \verb{$data}-slot of the output list.
The names are still in stored in \verb{$metadata$LABEL$name}.
You can disable this behaviour by \verb{setting use_weham_labels} to something
different than \code{TRUE}.
}
}
}
}

\section{Changes in version 1.2.2}{
\itemize{
\item Vignette published at \link{https://fvafrcu.gitlab.io/wehamr/doc/An_Introduction_to_wehamr.html}.
}
}

\section{Changes in version 1.2.1}{
\itemize{
\item \code{bulk_import_access_tables()} now returns a vector of paths to the exported
files with attribute \code{path} to  querry via \code{get_path()}.
\item Added a vignette.
}
}

\section{Changes in version 1.2.0}{
\itemize{
\item New functions \code{is_32bit()} and \code{is_windows()}.
\item \code{provide_weham_templates()} now provides input, models and ini as well. No
need to call \code{system.file()} directly.
\item New function \code{bulk_import_access_tables()}, based on Hendrik's proposal.
\item New function \code{get_weham_tables_of_interest()} as a helper for
\code{bulk_import_access_tables()}.
\item New function \code{bulk_export_access_tables()}, based on Hendrik's proposal.
}
}

\section{Changes in version 1.1.0}{
\itemize{
\item New functions \code{get_ini_attribute()} and \code{set_ini_attribute()} to help
manipulate weham.ini files.
\item New functions \code{read_ini()}, \code{write_ini()} and \code{get_path()} to help
manipulate weham.ini files.
\item Exported functions \code{provide_weham_templates()}, \code{get_ini_template()}, \code{adapt_ini()}, \code{make_ini()}.
All of them are documented internally (i.e. not in the manual).
\item Marked documentation for \code{is_valid_primary_key()} and \code{query_weham_version()} as internal.
\item New function \code{create_weham_ini()} to provide an ini file that runs with
the examples.
\item New function \code{weham()}, a rewrite of \code{run_weham} that relies on a weham.ini
instead of data base paths. Should be easier! Deprecated \code{run_weham()}.
\item New internal function \code{check_ini()} to validate a weham.ini. WEHAM itself is
rather... well.
}
}

\section{Changes in version 1.0.0}{
\itemize{
\item Added weham 2.1.1 as inst/weham/weham2/weham.exe,
so wehamr now runs weham 3.0.1 or weham 2.1.1, both in batch mode.
}
}

\section{Changes in version 0.5.0}{
\itemize{
\item Added argument \code{is_msaccess2007} to functions \code{read_access_table}, \code{write_access_table} and \code{run_weham} to
allow for newer versions of MS ACCESS than MS ACCESS 2007.
\item Removed argument \code{auto_assign} from \code{read_access_table()}.
\item Now exporting functions \code{read_access_table} and \code{write_access_table}.
\item Added function \code{is_valid_primary_key} to check whether some set of columns is
a valid primary key for a \code{data.frame}.
}
}

\section{Changes in version 0.4.0}{
\preformatted{Cleaned WPKSKW remainders

R/internal.R contains the single survivor from R/misc.R, list2data.frame().

renamed:    R/steuer.R -> R/control.R
deleted:    R/ggplot2.R
new file:   R/internal.R
deleted:    R/misc.R
deleted:    R/multiplot.R
deleted:    R/test_data.R
deleted:    R/volume.R
deleted:    R/wrap_weham.R
}
}

\section{Changes in version 0.3.1}{
\itemize{
\item Added function \code{query_weham_version} which queries the version of the WEHAM exe provided.
\item Added function \code{provide_weham_templates}. It returns the paths to templates for control,
output and output-sql databases fitting for weham versions 2 and 3 accordingly.
}
}

\section{Changes in version 0.3.0}{
\itemize{
\item Now writing weham.ini to output dircetory.
\item Now coming with all files needed to run weham (3.0.1).
\item Changed former arguments to \code{run_weham} from \code{run_dir} to \code{output_directory} and
from \code{weham_root} to \code{run_dir}, which is hopefully more sane.
}
}

\section{Changes in version 0.2.0}{
\itemize{
\item Default standard run works.
}
}

\section{Changes in version 0.1.0}{
\itemize{
\item Added a \code{NEWS.md} file to track changes to the package.
}
}

