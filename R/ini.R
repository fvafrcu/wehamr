#' @family \file{weham.ini} functions
#' @method print weham    
#' @export
print.weham <- function(x, ...) {
    attr(x, "class") <- NULL
    attr(x, "path") <- NULL
    print.default(x, ...)
}


#' Get the \code{path} Attribute from an object
#' 
#' We set paths on some objects, this is a convenience wrapper to
#' \code{\link{attr}}.
#' @param x An object.
#' @return The value of \code{attr(x, "path")}.
#' @family \file{weham.ini} functions
#' @export
#' @examples
#' get_path(read_ini())
#' get_path(3)
get_path <- function(x) {
    return(attr(x, "path"))
}

#' Read a \file{WEHAM.ini} 
#'
#' @param path The path to \file{weham.ini} file (or its directory).
#' @return A character giving the lines of the file. Has \code{attr} "path".
#' @export
#' @family \file{weham.ini} functions
#' @examples
#' print(ini <- read_ini())
read_ini <- function(path = system.file("weham", "Weham.ini", package = "wehamr")) {
  if (identical(tolower(basename(path)), "weham.ini")) {
    ini_file <- path
  } else {
    ini_file <- list.files(path, pattern = "weham.ini", full.names = TRUE, ignore.case = TRUE)
  }
  if (! file.exists(ini_file))
    throw(paste("File ", ini_file, "does not exists!"))
  ini <- readLines(ini_file)
  class(ini) <- c( "weham", "ini", class(ini))
  attr(ini, "path") <- ini_file
  return(ini)
}


#' Write a \code{WEHAM.ini} to Disk
#' 
#' @param x The R representation of a \file{WEHAM.ini}, see
#' \code{\link{read_ini}}.
#' @param path Path on Disk (either a file or a directory where \file{WEHAM.ini}
#' should be created. If \code{\link{NULL}}, the path is read from the
#' attributes of \code{x}.
#' @return TRUE on success, an error otherwise. 
#' But see \strong{Examples:} for code{\link{get_path}}.
#' @export
#' @family \file{weham.ini} functions
#' @examples
#' r <- write_ini(read_ini(), path = file.path(tempdir()))
#' print(r)
#' get_path(r)
write_ini <- function(x, path = NULL) {
    status <- FALSE
    checkmate::assert_true(all(c("weham", "ini") %in% class(x)))
    if (is.null(path)) {
        ini_file <- get_path(x)
    } else {
        if (identical(tolower(basename(path)), "weham.ini")) {
            ini_file <- path
        } else {
            ini_file <- file.path(path, "weham.ini")
        }
    }
    status <- writeLines(x, con  = ini_file)
    if (is.null(status)) {
        # NULL is the return value of writeLines
        status <- TRUE
    }
    attr(status, "path") <- normalizePath(ini_file)
    class(status) <- c("weham", class(status))
    return(status)

}


#' Get an Attribute From a \code{weham.ini}
#' 
#' @section Attributes:
#' See WEHAM documentation for the attributes in \code{weham.ini}.
#' @param x The \code{weham.ini} object.
#' @param attribute The name of the attribute. This is possibly all strings of a
#' \file{weham.ini} that start a line and end at an equal sign ("=").
#' @return The value of the attribute.
#' @export
#' @family \file{weham.ini} functions
#' @examples
#' ini <- read_ini()
#' get_ini_attribute(ini, "WEHAM_Steuer")
#' get_ini_attribute(ini, "WEHAM_Modelle")
#' get_ini_attribute(ini, "DefaultDir")
#' get_ini_attribute(ini, "WEHAM_INPUT")
get_ini_attribute <- function(x, attribute) {
    checkmate::assert_true(all(c("weham", "ini") %in% class(x)))
    checkmate::qassert(attribute, "S1")
    pattern <- paste0("^", attribute, "=")
    value <- sub(pattern, "", grep(pattern = pattern, x, value = TRUE))
    if (identical(attribute, "WEHAM_INPUT")) {
        # get rid of the driver specs. 
        value <- sub("^DBQ=(.*);.*", "\\1", value)
        # And there is actually an weham_input.mdb in that line,
        # although weham::wehamDlg.cpp reads
        # WehamSim.CSWehamInput_DB = WehamSim.CSWehamInput_VZ + "WEHAM_INPUT.MDB"
        # wtf???
        # WARNING: I do not use dirname as this dependes on the path to be valid
        # for the current os. And I am developing a lot on non-windows...
        value <- sub("weham_input.mdb$", "", value)
    }
    if (identical(length(value), 0L)) {
        warning("Could not find attribute `", attribute
                , "` in ", substitute(x), "!")
        value <- NULL
    }

    return(value)
}

#' Set an Attribute of a \code{weham.ini} to a Value
#'
#' @section Note:
#' Setting \code{value} to \code{\link{NULL}} will just return \code{x}.
#' This is needed for wrapping this helper function in
#' \code{\link{create_weham_ini}}.
#' @inheritParams get_ini_attribute
#' @inheritSection get_ini_attribute Attributes
#' @param value The new value for the attribute. 
#' @param use_mdb Use the path to an mdb file? By default, only the directories
#' containing the mdb files will be used, as was necessary for weham up to
#' versions 2.1.1 and 3.0.1.
#' @return The the new object
#' @export
#' @family \file{weham.ini} functions
#' @examples
#' ini <- read_ini()
#' print(v <- get_ini_attribute(x = ini, attribute = "WEHAM_INPUT"))
#' ini <- set_ini_attribute(ini, "WEHAM_INPUT", value = file.path(v, "foo"))
#' print(get_ini_attribute(ini, "WEHAM_INPUT"))
#' ini <- set_ini_attribute(ini, "WEHAM_Steuer", value = system.file("weham", "weham_steuer42.mdb", package = "wehamr"))
set_ini_attribute <- function(x, attribute, value, use_mdb = FALSE) {
    checkmate::assert_true(all(c("weham", "ini") %in% class(x)))
    checkmate::qassert(attribute, "S1")
    checkmate::assert_string(value, null.ok = TRUE)
    if (!is.null(value)) {
        if (grepl("\\.mdb", value)) {
            if (attribute == "WEHAM_Steuer") {
                message("Setting option weham_control_name to ",
                        basename(value), " which by default will be read ",
                        "by internal function `guess_control()`. ",
                        "Set its argument `try_option` to FALSE to ignore.")
                options("weham_control_name" = basename(value))
            }
            if (! isTRUE(use_mdb)) {
                # There must be no mdb files in weham.ini, only their directories!
                value <- dirname(value)
            }
        }
        if (grepl("^.:\\\\", value) || grepl("^.:/", value) || grepl("^/", value)) {
            # value is probably a path.
            if (!grepl("\\.mdb$", value) && !isTRUE(use_mdb)) {
                value <- paste0(value, "\\")
            }
            value <- normalizePath(value, mustWork = FALSE)
        }
        if (identical(attribute, "WEHAM_INPUT")) {
            # see get_ini_attribute()
            if (isTRUE(use_mdb)) {
                value <- paste0("DBQ=", value, 
                                ";DRIVER=", 
                                "Microsoft Access Driver (*.mdb)")
            } else {
                value <- paste0("DBQ=", value, 
                                "weham_input.mdb;DRIVER=", 
                                "Microsoft Access Driver (*.mdb)")
            }
        }
        pattern <- paste0("^", attribute, "=")
        value <- paste0(sub("\\^", "", pattern), value)
        x[grep(pattern, x)] <- value
    }
    return(x)
}

#' Write a \file{weham.ini} to Disk
#'
#' This is just convenience a wrapper to \code{\link{read_ini}},
#' \code{\link{set_ini_attribute}} and \code{\link{write_ini}}.
#' @param path Path to the directory where \file{weham.ini} should be written
#' to.
#' @param ... Arguments passed to \code{\link{set_ini_attribute}}. 
#' Pass `default = TRUE` to get a set of arguments suitable for files packaged 
#' with \pkg{wehamr}.
#" See \strong{Examples:}.
#' @return A path.
#' @export
#' @family \file{weham.ini} functions
#' @examples
#' i1 <- create_weham_ini()
#' read_ini(i1)
#' # Set to values for packages files:
#' pwt <- provide_weham_templates # get a shorter alias
#' exe <- system.file("weham", "weham_Klima.exe", package = "wehamr")
#' i2 <- create_weham_ini(WEHAM_Steuer = pwt(exe, "control"),
#'                        WEHAM_INPUT = pwt(exe, "input"),
#'                        WEHAM_Modelle = pwt(exe, "model"),
#'                        DefaultDir = tempdir()
#'                        )
#' read_ini(i2)
#' # Same as above using default = TRUE
#' identical(read_ini(i2), read_ini(create_weham_ini(default = TRUE)))
#' i3 <- create_weham_ini(WEHAM_Steuer = pwt(exe, "control"),
#' WEHAM_INPUT = pwt(exe, "input"),
#' WEHAM_Modelle = pwt(exe, "model"),
#' DefaultDir = tempdir(),
#' Klima = "true",
#' Start = "2030"
#' )
#' \dontrun{
#' file.show(i3)
#' }

#' 
create_weham_ini <- function(path = tempdir(), ...) {
  dots <- list(...)
  if (identical(dots$default, TRUE)) {
      pwt <- provide_weham_templates # get a shorter alias
      exe <- system.file("weham", "weham_Klima.exe", package = "wehamr")
      dots <- list(WEHAM_Steuer = pwt(exe, "control"),
                   WEHAM_INPUT = pwt(exe, "input"),
                   WEHAM_Modelle = pwt(exe, "model"),
                   DefaultDir = tempdir()
                   )
  }
  ini <- read_ini()
  for (i in seq(along = dots)) {
      ini <- set_ini_attribute(ini, names(dots[i]), dots[[i]])
  }

  if (grepl("weham.ini$", tolower(path))) {
    output_directory <- dirname(path)
  } else {
    output_directory <- path
  }
  dir.create(output_directory, recursive = TRUE, showWarnings = FALSE)
  res <- write_ini(ini, path)
  res <- get_path(res)
  return(res)
}
