# wehamr 1.5.0.9000

* FIXME

# wehamr 1.5.0

* Added `clean_weham()` to remove output and working files.
* We now import `fritools::is_windows()`.
* `read_access_table()` will now import the whole database into a named list if
  you pass `table_name = NULL`.
* Removed gitlab-ci, as gitlab will not run WEHAM, so the continuous integration
  will always fail.
* Now function `bulk_export_access_tables()` throws an error if you changed more
  than one table _and_ pass a primary key.
* Fixed link in documentation for `weham()`.


# wehamr 1.4.0

* Now using weham3.1.2!
  WEHAM now reads full file paths from it's ini file, allowing for different
  names for input, control and model like 
  _WEHAM\_Steuer=c:/tmp/foo.mdb_, as long as the end in _.mdb_.
  The ini file itself must no longer be  _./weham.ini_ but may be 
  _/path/to/some/file_.
  See  [https://git-dmz.thuenen.de/cullmann/weham//-/blob/master/NEWS.md](https://git-dmz.thuenen.de/cullmann/weham//-/blob/master/NEWS.md) 
  for details on weham3.1.2


# wehamr 1.3.0


*The backward compatiblity for functions `bulk_import_access_tables()` and
`bulk_import_access_tables()` introduced in 1.2.0 is broken!*

## Changed Interfaces 
* `bulk_import_access_tables()` does not write to `.GlobalEnv` anymore, thus returning a list of data and meta data.
  output of `bulk_import_access_tables()`.
* `bulk_export_access_tables()` now reads the changed return value from `bulk_import_access_tables()` `bulk_import_access_tables()`.

## The Name of the Control
The (base)name of the control database ("weham\_steuerXX.mdb") needs to be passed to
WEHAM, it cannot be specified in file "weham.ini" and may vary in "XX", in
contrary to the input database and the model database which invariably need to
be "weham\_input.mdb" and "weham\_modell.mdb".
Now `guess_control()` (called by `weham()` by default) tries to read an option 
"weham_control_name" by default. This option is set by
`set_ini_attribute()` (called by `create_weham_ini()`) 
if the attribute set is "WEHAM_Steuer". 
To specify the name of the control database you can
- pass it directly to `weham()` via the argument `control_name`,
- set the option "weham_control_name" and stick with the default argument
  `control_name` in the call to `weham()`. 
- set the attribute "WEHAM_Steuer" via `set_ini_attribute()` (possibly via
  `create_weham_ini()` and sticking with the default argument
  `control_name` in the call to `weham()`. 



## Minor Changes
* New convenience functions `get_data()` and `get_metadata()` to digest the
* Removed argument `verbose` from `bulk_import_access_tables()` 
* Now `bulk_import_access_tables()` by default labels weham databases:
  - weham\_steuerXX is labeled "control"
  - weham\_input is labeled "input"
  - weham\_modell is labeled "model"
  The labels are use as names in the `$data`-slot of the output list.
  The names are still in stored in `$metadata$LABEL$name`.
  You can disable this behaviour by `setting use_weham_labels` to something
  different than `TRUE`.
  

# wehamr 1.2.2

* Vignette published at [https://fvafrcu.gitlab.io/wehamr/doc/An_Introduction_to_wehamr.html].

# wehamr 1.2.1

* `bulk_import_access_tables()` now returns a vector of paths to the exported
  files with attribute `path` to  querry via `get_path()`.
* Added a vignette.

# wehamr 1.2.0

* New functions `is_32bit()` and `is_windows()`.
* `provide_weham_templates()` now provides input, models and ini as well. No
  need to call `system.file()` directly.
* New function `bulk_import_access_tables()`, based on Hendrik's proposal.
* New function `get_weham_tables_of_interest()` as a helper for
  `bulk_import_access_tables()`.
* New function `bulk_export_access_tables()`, based on Hendrik's proposal.

# wehamr 1.1.0
* New functions `get_ini_attribute()` and `set_ini_attribute()` to help
  manipulate weham.ini files.
* New functions `read_ini()`, `write_ini()` and `get_path()` to help
  manipulate weham.ini files.
* Exported functions `provide_weham_templates()`, `get_ini_template()`, `adapt_ini()`, `make_ini()`.
  All of them are documented internally (i.e. not in the manual).
* Marked documentation for `is_valid_primary_key()` and `query_weham_version()` as internal.
* New function `create_weham_ini()` to provide an ini file that runs with
  the examples.
* New function `weham()`, a rewrite of `run_weham` that relies on a weham.ini
  instead of data base paths. Should be easier! Deprecated `run_weham()`.
* New internal function `check_ini()` to validate a weham.ini. WEHAM itself is
  rather... well.

# wehamr 1.0.0

* Added weham 2.1.1 as inst/weham/weham2/weham.exe,
  so wehamr now runs weham 3.0.1 or weham 2.1.1, both in batch mode.

# wehamr 0.5.0
* Added argument `is_msaccess2007` to functions `read_access_table`, `write_access_table` and `run_weham` to
  allow for newer versions of MS ACCESS than MS ACCESS 2007.
* Removed argument `auto_assign` from `read_access_table()`.
* Now exporting functions `read_access_table` and `write_access_table`.
* Added function `is_valid_primary_key` to check whether some set of columns is
  a valid primary key for a `data.frame`.

# wehamr 0.4.0
    Cleaned WPKSKW remainders
    
    R/internal.R contains the single survivor from R/misc.R, list2data.frame().
    
    renamed:    R/steuer.R -> R/control.R
    deleted:    R/ggplot2.R
    new file:   R/internal.R
    deleted:    R/misc.R
    deleted:    R/multiplot.R
    deleted:    R/test_data.R
    deleted:    R/volume.R
    deleted:    R/wrap_weham.R
# wehamr 0.3.1
- Added function `query_weham_version` which queries the version of the WEHAM exe provided.
- Added function `provide_weham_templates`. It returns the paths to templates for control, 
   output and output-sql databases fitting for weham versions 2 and 3 accordingly.

# wehamr 0.3.0
- Now writing weham.ini to output dircetory.
- Now coming with all files needed to run weham (3.0.1).
- Changed former arguments to `run_weham` from `run_dir` to `output_directory` and
  from `weham_root` to `run_dir`, which is hopefully more sane. 

# wehamr 0.2.0
- Default standard run works.

# wehamr 0.1.0

* Added a `NEWS.md` file to track changes to the package.



