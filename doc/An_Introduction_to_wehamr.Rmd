---
title: "An Introduction to *wehamr*"
author: 
date: 2021-10-12, 15:03:28
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{An Introduction to wehamr }
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

# Introduction
WEHAM is a forest forecasting system developed for
the german national forest inventory. 
```{r}
library("wehamr")
```

Its basic usage is simple:

``` {r}
output <- weham()
```
Have a look at the files created, then clean them up:
``` {r}
list.files(dirname(output))
clean_weham(output)
list.files(dirname(output))
```

# Getting Default Data
`wehamr` comes with default data to run weham. It is not what you want, but works for examples.


```{r}
# Get default weham data
import <- bulk_import_access_tables(bulk = NULL)
# Write that data to disk
system.time(mdb <- bulk_export_access_tables(import))
print(modification_times <- file.info(list.files(get_path(mdb), full.names = TRUE))["mtime"])
```

# Simply Running WEHAM
Suppose the default data we stored to disk were real data, then we need to set up 
an appropriate file `weham.ini`.


```{r}
ini_file <- suppressMessages(create_weham_ini(default = TRUE))

```
We could edit this file with any editor at hand, but we can also read, change and write the file
from within R. First, we read the file contents:

``` {r}
ini <- read_ini(ini_file)
```

Then we change some attributes of weham.ini to point to the data we 
stored on disk above and write the ini file back to disk:

``` {r}
ini <- set_ini_attribute(ini,  "WEHAM_Modelle", mdb["model"])
ini <- set_ini_attribute(ini,  "WEHAM_Steuer", mdb["control"])
ini <- set_ini_attribute(ini,  "WEHAM_INPUT", mdb["input"])
write_ini(ini)

```
We can look at the ini file 

``` {r, eval = FALSE}
file.show(get_path(ini))

```
or simply run WEHAM and look at the working directory:

``` {r}
out <- weham(ini_file = ini_file)

list.files(get_path((out)))
```

Had we used `shell.exec(get_path((out)))`, we could double click the weham
executable in this directory to manually rerun WEHAM if we wanted to.


# Re-importing the Data
We typically will _not_ use the default data coming with the package, so we re-import the data as if `mdb` would be pointing to files in some directory under, say, H:/FVA-Projekte/Your-Project/.

```{r}
# re-import that data
new_bulk <- stats::setNames(list(get_weham_tables_of_interest("input"),
                                 get_weham_tables_of_interest("control"),
                                 get_weham_tables_of_interest("model")),
                                 c(mdb[["input"]], mdb[["control"]], mdb[["model"]]))
import <- bulk_import_access_tables(bulk = new_bulk)

```

If the data didn't change, nothing much is done:
```{r}
# no re-write for unchanged data:
system.time(path <- get_path(bulk_export_access_tables(import, root = get_path(mdb), verbose = TRUE)))
all(file.info(list.files(path, full.names = TRUE))["mtime"] == modification_times)
```

Now we change some data, and only the database containing that table changes.
```{r}
# change some data
import$data$input$WEHAMM_x_ba[2,1] <- 9
#  to re-write the unchanged data to disk
system.time(path <- get_path(bulk_export_access_tables(import, root = path, verbose = TRUE)))
# one db changed:
print(modification_times <- file.info(list.files(path, full.names = TRUE))["mtime"])
```

# Why all the hassle?
Right now you have three files of about 50 MB written to your disk:

```{r}
print(files <- list.files(dirname(get_path(mdb)),
                          full.names = TRUE, pattern = ".*\\.mdb", 
                          recursive = TRUE))
R.utils::hsize(sum(file.size(files)))
```


Suppose we want to run weham in a loop over some value. 
We set the root to the output directory to `get_path(mdb)`.


```{r}
for (v in seq(80, 100, 10)) {
    import$data$control$WEHAMS_DF_Z_Modell$Zielst_Eproz <- v
    mdb <- bulk_export_access_tables(import, root = get_path(mdb),
                                      primary_key = c("Land", "EigentId", "DFBArtId", "Jahr", "Altervon"))
    # create a weham.ini from the changed data and run weham 
    ini <- create_weham_ini(WEHAM_Steuer = mdb[["control"]],
                            WEHAM_INPUT = mdb[["input"]],
                            WEHAM_Modelle = mdb[["model"]])
    weham(ini_file = ini)
    
}

```
We overwrite existing control-db in each run of the loop, input and models stay untouched:
```{r}
file.info(list.files(path, full.names = TRUE))["mtime"]
```

And we get the same amount of disk space:
```{r}
print(files <- list.files(dirname(get_path(mdb)),
                          full.names = TRUE, pattern = ".*\\.mdb", 
                          recursive = TRUE))
R.utils::hsize(sum(file.size(files)))
```

Now you do not set the root directory or set it to some place other than the folder containing the mdb:

```{r}
for (v in seq(80, 100, 10)) {
    import$data$control$WEHAMS_DF_Z_Modell$Zielst_Eproz <- v
    mdb <- bulk_export_access_tables(import,
                                      primary_key = c("Land", "EigentId", "DFBArtId", "Jahr", "Altervon"))
    # create a weham.ini from the changed data and run weham 
    ini <- create_weham_ini(WEHAM_Steuer = mdb[["control"]],
                            WEHAM_INPUT = mdb[["input"]],
                            WEHAM_Modelle = mdb[["model"]])
    weham(ini_file = ini)
    
}

```
Now you get a lot of tables cluttering your disk:
```{r}
print(files <- list.files(dirname(get_path(mdb)),
                          full.names = TRUE, pattern = ".*\\.mdb", 
                          recursive = TRUE))
R.utils::hsize(sum(file.size(files)))
```

