
#########################################################################
# modifying access tables for later wehamr input

# @results.dir specify the path for a new folder "Wehamr_Tables". If not specified, a folder will be crated at paste(Sys.getenv("HOME"), "Documents", sep="/")
wehamr_launchpad <- function(results.dir){
  
  #create new directory "wehamr_Tables", if it does not already exist
  if(missing(results.dir)) {
    Default.path <- paste(Sys.getenv("HOME"), "Documents", sep="/")
  }else{
    Default.path <- results.dir
  }
  dir.name <- "Wehamr_Tables"
  outputDIR <- file.path(Default.path, dir.name)
  if (!dir.exists(outputDIR)) {dir.create(outputDIR)}

  # creating a sub-directory name from date() as permanent unique repository for wehamr input (and output?)
  time.vec <- unlist(strsplit(date(), split=" "))# need to have this additional check since on MacOS date() produces additional "_"
  time.vec <- time.vec[time.vec != c("")]
  for(i in 1:2) {
    if(1==1) zeit.i <- sub(pattern=":", replacement="", x=time.vec[4]) #seconds
    if(i==2) zeit <- sub(pattern=":", replacement="", x=zeit.i)
  }
  
  name.timestamp <- paste("Wehamr_Tables", paste(time.vec[5], # weekday
                                                 time.vec[2], # month
                                                 time.vec[3], # day 
                                                 zeit,
                                                 sep="_"), 
                          sep="_")
  
  subDIR <- file.path(Default.path, dir.name, name.timestamp)
  if (!dir.exists(subDIR)) {dir.create(subDIR)}
  
  # copying original access DBs in new dir name.timestamp
  DB.vec <- c("weham_input", "weham_modell", "weham_steuer42")
  for(i in 1:length(DB.vec)){
    path.from <- system.file("weham", paste(DB.vec[i], "mdb", sep=c(".")), package = "wehamr")
    path.to <- subDIR
    file.copy(from=path.from, to=path.to, overwrite = TRUE, recursive = FALSE, copy.mode = TRUE, copy.date = FALSE)
  }
  
  # Update access files with Access.tables from R workspace (as loaded by function "access_table_import")
  for(i in 1:length(DB.vec)){ 
    if(DB.vec[i]=="weham_input") {Tab.vec <- c("WEHAMI_WZP", "WEHAMI_Eck", "WEHAMI_x_ba")}
    if(DB.vec[i]=="weham_modell") {Tab.vec <- c("WEHAMM_DF_Modelle", "WEHAM_DFArt", "WEHAMM_Bonitaet_mh", 
                                                "WEHAMM_BAStartwerte", "WEHAMM_MortGr", "WEHAMM_Vorrat",
                                                "WEHAMM_W_Modelle", "WEHAMM_WFunktionen", "WEHAMM_x_ba")}
    if(DB.vec[i]=="weham_steuer42") {Tab.vec <- c("WEHAMS_DZ_Z_Modell", "WEHAMS_Profil", "WEHAMS_Sortierung", "WEHAMS_W_Z_Modell")}
    for(j in 1:length(Tab.vec)){
      pathtoAccTab <- paste(path.to, "/" ,DB.vec[i], ".mdb", sep=c(""))
      channel <- RODBC::odbcConnectAccess(pathtoAccTab)
      RODBC::sqlUpdate(channel = channel, dat = Tab.vec[j], tablename = "WEHAMM_W_Modelle", index=c("WModellId"))
      RODBC::odbcCloseAll()
    }
  }
}# end function